import 'package:flutter/cupertino.dart';
import 'package:pc_build_assistant/models/pc_component.dart';

class PCGraphicsProcessor extends PCComponent {
  final int gpuSize;
  final int performance;
  final int power;
  final int powerConnectors;

  PCGraphicsProcessor({
    String manufacturer,
    String name,
    String description,
    String imgurl,
    @required this.gpuSize,
    @required this.performance,
    @required this.power,
    @required this.powerConnectors,
  }) : super(
          manufacturer: manufacturer,
          name: name,
          description: description,
          imgurl: imgurl,
        );
}
