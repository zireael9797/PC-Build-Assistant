import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:pc_build_assistant/models/pc_chs.dart';
import 'package:pc_build_assistant/models/pc_component.dart';
import 'package:pc_build_assistant/models/pc_cpu.dart';
import 'package:pc_build_assistant/models/pc_gpu.dart';
import 'package:pc_build_assistant/models/pc_mbd.dart';
import 'package:pc_build_assistant/models/pc_psu.dart';
import 'package:pc_build_assistant/models/pc_ram.dart';
import 'package:pc_build_assistant/models/pc_str_m2.dart';
import 'package:pc_build_assistant/models/pc_str_sata.dart';

class DatabaseHelper {
  static Firestore _database = Firestore.instance;

  static getData() async {
    List<PCComponent> components = new List<PCComponent>.empty(growable: true);
    try {
      List<Map<String, dynamic>> componentList;
      QuerySnapshot collection =
          await _database.collection('pc-components').getDocuments();
      List<DocumentSnapshot> documents = collection.documents;
      componentList = documents.map((DocumentSnapshot snapshot) {
        return snapshot.data;
      }).toList();
      components.clear();
      for (Map<String, dynamic> item in componentList) {
        switch (item["type"]) {
          case "cpu":
            components.add(
              PCProcessor(
                manufacturer: item["manufacturer"],
                name: item["name"],
                description: item["description"],
                imgurl: item["imgurl"],
                socket: item["socket"],
                performance: item["performance"],
                power: item["power"],
              ),
            );
            break;
          case "gpu":
            components.add(
              PCGraphicsProcessor(
                manufacturer: item["manufacturer"],
                name: item["name"],
                description: item["description"],
                imgurl: item["imgurl"],
                gpuSize: item["gpusize"],
                performance: item["performance"],
                power: item["power"],
                powerConnectors: item["powerconnectors"],
              ),
            );
            break;
          case "chs":
            components.add(
              PCChassis(
                  manufacturer: item["manufacturer"],
                  name: item["name"],
                  description: item["description"],
                  imgurl: item["imgurl"],
                  size: item["size"],
                  gpusize: item["gpusize"]),
            );
            break;
          case "mbd":
            components.add(
              PCMotherboard(
                manufacturer: item["manufacturer"],
                name: item["name"],
                description: item["description"],
                imgurl: item["imgurl"],
                size: item["size"],
                ramBus: item["rambus"],
                ramCapacity: item["ramcapacity"],
                ramSlots: item["ramslots"],
                ramType: item["ramtype"],
                socket: item["socket"],
                sataSlots: item["sataslots"],
              ),
            );
            break;
          case "psu":
            components.add(
              PCPowerSupply(
                manufacturer: item["manufacturer"],
                name: item["name"],
                description: item["description"],
                imgurl: item["imgurl"],
                power: item["power"],
                powerConnectors: item["powerconnectors"],
              ),
            );
            break;
          case "ram":
            components.add(
              PCRam(
                manufacturer: item["manufacturer"],
                name: item["name"],
                description: item["description"],
                imgurl: item["imgurl"],
                size: item["size"],
                ramType: item["ramtype"],
                ramBus: item["rambus"],
              ),
            );
            break;
          case "str_sata":
            components.add(
              PCStorageSata(
                manufacturer: item["manufacturer"],
                name: item["name"],
                description: item["description"],
                imgurl: item["imgurl"],
                capacity: item["capacity"],
              ),
            );
            break;
          case "str_m2":
            components.add(
              PCStorageM2(
                manufacturer: item["manufacturer"],
                name: item["name"],
                description: item["description"],
                imgurl: item["imgurl"],
                capacity: item["capacity"],
              ),
            );
            break;
          default:
            print("Unknown Component");
            break;
        }
      }
    } catch (e) {
      print(e.toString());
    }
    return components;
  }
}
