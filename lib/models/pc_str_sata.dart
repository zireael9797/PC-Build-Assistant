import 'package:pc_build_assistant/models/pc_str.dart';

class PCStorageSata extends PCStorage {
  PCStorageSata({
    String manufacturer,
    String name,
    String description,
    String imgurl,
    int capacity,
  }) : super(
          manufacturer: manufacturer,
          name: name,
          description: description,
          imgurl: imgurl,
          capacity: capacity,
        );
}
