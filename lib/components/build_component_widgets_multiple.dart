import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:pc_build_assistant/components/build_component_widgets_entry.dart';
import 'package:pc_build_assistant/models/pc_component.dart';
import 'package:pc_build_assistant/util/constants.dart';

class BuildComponentWidgetsMultiple extends StatefulWidget {
  final List<PCComponent> components;
  final OnComponentEntryRemove onRemoveEntry;
  final String title;
  BuildComponentWidgetsMultiple(
      {Key key,
      @required this.components,
      this.title = "Title",
      this.onRemoveEntry})
      : super(key: key);

  @override
  _BuildComponentWidgetsMultipleState createState() =>
      _BuildComponentWidgetsMultipleState();
}

class _BuildComponentWidgetsMultipleState
    extends State<BuildComponentWidgetsMultiple> with TickerProviderStateMixin {
  List<PCComponent> _currentComponents;
  List<Widget> _listEntries = [];
  @override
  void initState() {
    _currentComponents = widget.components;
    _updateComponents();
    super.initState();
  }

  void _updateComponents() {
    List<Widget> newEntries = [];
    for (int i = 0; i < _currentComponents.length; i++) {
      newEntries.add(
        BuildComponentWidgetEntry(
          title: "meh",
          component: _currentComponents[i],
          index: i,
          onRemove: (component, index) {
            print(index);
            widget.onRemoveEntry(component, index);
            _updateComponents();
          },
        ),
      );
    }
    setState(() {
      _listEntries = newEntries;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: kComponentBoxColor,
        boxShadow: [
          BoxShadow(
            color: Color(0x44000000),
            blurRadius: 6,
            spreadRadius: 3,
          ),
        ],
        borderRadius: BorderRadius.all(
          Radius.circular(kRadius),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            // TITLE
            padding: EdgeInsets.all(10),
            child: Center(
              child: Text(
                widget.title,
                style: kAnimatedTextStyle,
              ),
            ),
          ),
          AnimatedSize(
            vsync: this,
            duration: Duration(milliseconds: 100),
            child: AnimatedSwitcher(
              duration: Duration(milliseconds: 300),
              child: _currentComponents.length > 0
                  ? Column(
                      key: Key(widget.title),
                      //EVERYTHING Besides the TITLE
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: _listEntries, //The entries go here
                    )
                  : Container(
                      key: Key("no" + widget.title),
                      //When there's no component of this type
                      padding: EdgeInsets.all(10),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Icon(
                                Icons.help,
                                size: 40,
                                color: Colors.red,
                              ),
                            ),
                            Text(
                              "Not Added",
                              style: TextStyle(
                                  fontSize: 20, color: kRedButtonColor),
                            ),
                          ],
                        ),
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
