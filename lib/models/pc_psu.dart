import 'package:flutter/cupertino.dart';
import 'package:pc_build_assistant/models/pc_component.dart';

class PCPowerSupply extends PCComponent {
  final int power;
  final int powerConnectors;

  PCPowerSupply({
    String manufacturer,
    String name,
    String description,
    String imgurl,
    @required this.power,
    @required this.powerConnectors,
  }) : super(
          manufacturer: manufacturer,
          name: name,
          description: description,
          imgurl: imgurl,
        );
}
