import 'package:pc_build_assistant/models/pc_chs.dart';
import 'package:pc_build_assistant/models/pc_cpu.dart';
import 'package:pc_build_assistant/models/pc_gpu.dart';
import 'package:pc_build_assistant/models/pc_mbd.dart';
import 'package:pc_build_assistant/models/pc_psu.dart';
import 'package:pc_build_assistant/models/pc_ram.dart';
import 'package:pc_build_assistant/models/pc_str_sata.dart';

class Build {
  PCChassis chassis;
  PCMotherboard motherboard;
  PCProcessor processor;
  PCGraphicsProcessor gpu;
  PCPowerSupply psu;
  List<PCRam> rams = [];
  List<PCStorageSata> satas = [];

  Build(
      {this.chassis,
      this.motherboard,
      this.processor,
      this.gpu,
      this.psu,
      rams,
      satas}) {
    if (rams != null) {
      this.rams = rams;
    }
    if (satas != null) {
      this.satas = satas;
    }
  }

  @override
  String toString() {
    return "$chassis, $motherboard, $processor, $gpu, $psu";
  }
}
