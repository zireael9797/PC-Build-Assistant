//import 'package:pc_build_assistant/models/pc_chs.dart';
import 'package:pc_build_assistant/models/build.dart';
import 'package:pc_build_assistant/models/pc_chs.dart';
import 'package:pc_build_assistant/models/pc_component.dart';
import 'package:pc_build_assistant/models/pc_cpu.dart';
import 'package:pc_build_assistant/models/pc_gpu.dart';
import 'package:pc_build_assistant/models/pc_mbd.dart';
import 'package:pc_build_assistant/models/pc_psu.dart';
import 'package:pc_build_assistant/models/pc_ram.dart';
import 'package:pc_build_assistant/models/pc_str_sata.dart';

class BuildManager {
  static List<String> errors = [];
  static List<String> testBuild() {
    errors.clear();
    _chassisCheck();
    _motherboardCheck();
    _processorCheck();
    _gpuCheck();
    _psuCheck();
    _ramCheck();
    _sataStorageCheck();
    return errors;
  }

  static Build build = Build();
  static void addComponent(PCComponent component) {
    if (component is PCGraphicsProcessor) {
      build.gpu = component;
      //print("GPU");
    } else if (component is PCProcessor) {
      build.processor = component;
      //print("CPU");
    } else if (component is PCPowerSupply) {
      build.psu = component;
      //print("PSU");
    } else if (component is PCMotherboard) {
      build.motherboard = component;
      //print("Motherboard");
    } else if (component is PCChassis) {
      build.chassis = component;
      //print("Chassis");
    } else if (component is PCRam) {
      build.rams.add(component);
      //print("Ram");
    } else if (component is PCStorageSata) {
      build.satas.add(component);
    }
  }

  static int getItemCount() {
    int count = 0;
    if (build.chassis != null) count++;
    if (build.motherboard != null) count++;
    if (build.processor != null) count++;
    if (build.gpu != null) count++;
    if (build.psu != null) count++;
    print("$count items");
    return count;
  }

  static getBuild() {}
  static removeComponent(PCComponent component) {
    if (component is PCGraphicsProcessor) {
      build.gpu = null;
    } else if (component is PCProcessor) {
      build.processor = null;
    } else if (component is PCPowerSupply) {
      build.psu = null;
    } else if (component is PCMotherboard) {
      build.motherboard = null;
    } else if (component is PCChassis) {
      build.chassis = null;
    }
    print(removeComponent);
  }

  static removeComponentEntry(PCComponent component, int index) {
    if (component is PCRam) {
      build.rams.removeAt(index);
    }
    if (component is PCStorageSata) {
      build.satas.removeAt(index);
    }
  }

  static getItem(int index) {
    int count = 0;
    if (build.chassis != null) {
      if (count == index) {
        return build.chassis;
      } else {
        count++;
      }
    }
    if (build.motherboard != null) {
      if (count == index) {
        return build.motherboard;
      } else {
        count++;
      }
    }
    if (build.processor != null) {
      if (count == index) {
        return build.processor;
      } else {
        count++;
      }
    }
    if (build.gpu != null) {
      if (count == index) {
        return build.gpu;
      } else {
        count++;
      }
    }
    if (build.psu != null) {
      if (count == index) {
        return build.psu;
      } else {
        count++;
      }
    }
  }

//----------------------------------------------BUILD CHECKERS--------------------------------------
  static void _chassisCheck() {
    if (build.chassis != null) {
    } else {
      errors.add("You must add a Chassis");
    }
  }

  static void _motherboardCheck() {
    if (build.motherboard != null) {
      if (build.chassis != null &&
          build.chassis.size < build.motherboard.size) {
        errors.add("Your Chassis is too small for your Motherboard");
      }
    } else {
      errors.add("You must add a Motherboard");
    }
  }

  static void _processorCheck() {
    if (build.processor != null) {
      if (build.motherboard != null &&
          (build.motherboard.socket != build.processor.socket)) {
        errors
            .add("Your Motherboard socket is incompatible with your Processor");
      }
    } else {
      errors.add("You must add a Processor");
    }
  }

  static void _gpuCheck() {
    if (build.gpu != null) {
      if (build.chassis != null &&
          (build.chassis.gpusize < build.gpu.gpuSize)) {
        errors.add("Your gpu is too long for your Chassis");
      }
      if (build.processor != null &&
          build.processor.performance * 0.9 < build.gpu.performance) {
        errors.add("Your processor may bottleneck your Graphics Card");
      }
    } else {
      errors.add("You haven't added a Graphics Card");
    }
  }

  static void _psuCheck() {
    if (build.psu != null) {
      int power = 0;
      if (build.gpu != null) {
        power = power + (build.gpu.power).toInt();
        if (build.gpu.powerConnectors > build.psu.powerConnectors)
          errors.add(
              "Your Power Supply doesn't have enough power connectors for your Graphics Card");
      }
      if (build.processor != null)
        power = power + (build.processor.power).toInt();
      if (build.psu.power * 0.8 < power) {
        errors.add("Your Power Supply might not be sufficient");
      }
    } else {
      errors.add("You must add a Power Supply");
    }
  }

  static void _ramCheck() {
    if (build.rams.length > 0) {
      String ramType = build.rams[0].ramType;
      bool sameRam = true;
      for (PCRam ram in build.rams) {
        if (ram.ramType != ramType) {
          errors.add("Your rams are of different types");
          sameRam = false;
          break;
        }
      }
      if (build.motherboard != null) {
        if (sameRam && ramType != build.motherboard.ramType) {
          errors
              .add("Your Ram's type doesn't match the Motherboard's Ram type");
        }
        int minBus = build.motherboard.ramBus;
        int maxBus = minBus;
        int size = 0;
        for (PCRam ram in build.rams) {
          size += ram.size;
          if (ram.ramBus < minBus) {
            minBus = ram.ramBus;
          } else if (ram.ramBus > maxBus) {
            maxBus = ram.ramBus;
          }
        }
        if (size > build.motherboard.ramCapacity) {
          errors.add("You have too much Ram");
        }
        if (minBus != maxBus) {
          errors.add(
              "Your Rams won't run at full speed due to incompatible ram speed with motherboard or rams");
        }
      }
    } else {
      errors.add("You must have atleast one Ram");
    }
  }

  static void _sataStorageCheck() {
    if (build.satas.length > 0) {
      if (build.motherboard != null) {
        if (build.motherboard.sataSlots < build.satas.length) {
          errors.add("You have too many storage devices for your Motherboard");
        }
      }
    } else {
      errors.add("You must have atleast one Storage device");
    }
  }
}
