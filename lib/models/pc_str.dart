import 'package:flutter/foundation.dart';
import 'package:pc_build_assistant/models/pc_component.dart';

class PCStorage extends PCComponent {
  final int capacity;

  PCStorage({
    String manufacturer,
    String name,
    String description,
    String imgurl,
    @required this.capacity,
  }) : super(
          manufacturer: manufacturer,
          name: name,
          description: description,
          imgurl: imgurl,
        );
}
