import 'package:flutter/cupertino.dart';
import 'package:pc_build_assistant/models/pc_component.dart';

class PCRam extends PCComponent {
  int ramBus;
  int size;
  String ramType;

  PCRam({
    String manufacturer,
    String name,
    String description,
    String imgurl,
    @required this.ramBus,
    @required this.ramType,
    @required this.size,
  }) : super(
          manufacturer: manufacturer,
          name: name,
          description: description,
          imgurl: imgurl,
        );
}
