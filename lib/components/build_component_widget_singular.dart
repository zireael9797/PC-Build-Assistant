import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:pc_build_assistant/models/pc_component.dart';
import 'package:pc_build_assistant/util/constants.dart';

typedef OnComponentRemove(PCComponent component);

class BuildComponentWidgetSingular extends StatefulWidget {
  final PCComponent component;
  final OnComponentRemove onRemove;
  final String title;
  BuildComponentWidgetSingular(
      {Key key, @required this.component, this.title = "Title", this.onRemove})
      : super(key: key);

  @override
  _BuildComponentWidgetSingularState createState() =>
      _BuildComponentWidgetSingularState();
}

class _BuildComponentWidgetSingularState
    extends State<BuildComponentWidgetSingular> with TickerProviderStateMixin {
  bool _removed; //keeps track of whether the component has been removed or not
  @override
  void initState() {
    _removed = widget.component != null
        ? false
        : true; // sets the initial value of widget
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: kComponentBoxColor,
        boxShadow: [
          BoxShadow(
            color: Color(0x44000000),
            blurRadius: 6,
            spreadRadius: 3,
          ),
        ],
        borderRadius: BorderRadius.all(
          Radius.circular(kRadius),
        ),
      ),
      child: Column(
        //Title and rest
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            // TITLE
            padding: EdgeInsets.all(10),
            child: Center(
              child: Text(
                widget.title,
                style: kAnimatedTextStyle,
              ),
            ),
          ),
          AnimatedSize(
            vsync: this,
            duration: Duration(milliseconds: 100),
            child: AnimatedSwitcher(
              duration: Duration(milliseconds: 300),
              child: !_removed
                  ? Column(
                      key: Key(widget.title),
                      //EVERYTHING Besides the TITLE
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Image(
                              image: CachedNetworkImageProvider(
                                  widget.component.imgurl),
                              width: 100,
                              height: 100,
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      widget.component.manufacturer,
                                      style: TextStyle(
                                          fontFamily: "Rodin", fontSize: 20),
                                    ),
                                    Text(
                                      widget.component.name,
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    widget.component.description,
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.circular(kRadius),
                                    color: kRegisterButtonColor,
                                  ),
                                  child: IconButton(
                                    color: Colors.white,
                                    icon: Icon(Icons.close),
                                    onPressed: () {
                                      if (widget.onRemove != null) {
                                        setState(() {
                                          _removed = true;
                                        });
                                        widget.onRemove(widget.component);
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    )
                  : Container(
                      key: Key("no" + widget.title),
                      //When there's no component of this type
                      padding: EdgeInsets.all(10),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Icon(
                                Icons.help,
                                size: 40,
                                color: Colors.red,
                              ),
                            ),
                            Text(
                              "Not Added",
                              style: TextStyle(
                                  fontSize: 20, color: kRedButtonColor),
                            ),
                          ],
                        ),
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
