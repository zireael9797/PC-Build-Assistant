import 'package:flutter/cupertino.dart';
import 'package:pc_build_assistant/models/pc_component.dart';

class PCProcessor extends PCComponent {
  final String socket;
  final int performance;
  final int power;

  PCProcessor({
    String manufacturer,
    String name,
    String description,
    String imgurl,
    @required this.socket,
    @required this.performance,
    @required this.power,
  }) : super(
          manufacturer: manufacturer,
          name: name,
          description: description,
          imgurl: imgurl,
        );
}
