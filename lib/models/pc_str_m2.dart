import 'package:pc_build_assistant/models/pc_str.dart';

class PCStorageM2 extends PCStorage {
  PCStorageM2({
    String manufacturer,
    String name,
    String description,
    String imgurl,
    int capacity,
  }) : super(
          manufacturer: manufacturer,
          name: name,
          description: description,
          imgurl: imgurl,
          capacity: capacity,
        );
}
