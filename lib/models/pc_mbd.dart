import 'package:flutter/cupertino.dart';
import 'package:pc_build_assistant/models/pc_component.dart';

class PCMotherboard extends PCComponent {
  final int size;
  final int ramCapacity;
  final int ramBus;
  final int ramSlots;
  final String ramType;
  final String socket;
  final int sataSlots;

  PCMotherboard({
    String manufacturer,
    String name,
    String description,
    String imgurl,
    @required this.size,
    @required this.ramCapacity,
    @required this.ramBus,
    @required this.ramSlots,
    @required this.ramType,
    @required this.socket,
    @required this.sataSlots,
  }) : super(
          manufacturer: manufacturer,
          name: name,
          description: description,
          imgurl: imgurl,
        );
}
