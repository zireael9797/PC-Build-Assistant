import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:pc_build_assistant/models/pc_component.dart';
import 'package:pc_build_assistant/util/constants.dart';

typedef OnComponentEntryRemove(PCComponent component, int index);

class BuildComponentWidgetEntry extends StatefulWidget {
  final PCComponent component;
  final OnComponentEntryRemove onRemove;
  final String title;
  final int index;
  BuildComponentWidgetEntry(
      {Key key,
      @required this.component,
      this.index,
      this.title = "Title",
      this.onRemove})
      : super(key: key);

  @override
  _BuildComponentWidgetEntryState createState() =>
      _BuildComponentWidgetEntryState();
}

class _BuildComponentWidgetEntryState extends State<BuildComponentWidgetEntry>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        key: Key(widget.title),
        //EVERYTHING Besides the TITLE
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image(
                image: CachedNetworkImageProvider(widget.component.imgurl),
                width: 100,
                height: 100,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        widget.component.manufacturer,
                        style: TextStyle(fontFamily: "Rodin", fontSize: 20),
                      ),
                      Text(
                        widget.component.name,
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(kRadius),
                  color: kRegisterButtonColor,
                ),
                child: IconButton(
                  color: Colors.white,
                  icon: Icon(Icons.close),
                  onPressed: () {
                    if (widget.onRemove != null) {
                      widget.onRemove(widget.component, widget.index);
                    }
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
